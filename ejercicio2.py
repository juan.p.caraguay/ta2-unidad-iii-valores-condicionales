# Reescribe el programa de salario usando try y except de modo que el programa sea capaz de gsetionar
# entradas no numericas con elegancia mostrando un mensaje y saliendo del programa
# a continuacion se muestras dos ejecucuiones del programa

# autor: = " juan caraguay"
# email:   = "juan.p.caraguay@unl.edu.ec"

try:
    horas = float(input("Introduzca las horas:"))
    tarifa= float(input("Introduzca la tarifa por hora:"))
    if horas >=40:
        horas_xt = horas - 40
        tarifa_xt = (0.5 * tarifa) + tarifa
        salario = (tarifa_xt * horas_xt) + (horas * tarifa)
        print('su salario es de: ' , salario)
    else:
        salario = tarifa * horas
        print('su salario es de: ' , salario)
except:
    print('error ´por favor Introduzca un numero')