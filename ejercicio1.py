# Escribe un programa de calculo de un salario para darle al empleado
# 1.5 veces la tarifa horaria para todas las horas trabajadas que excedan de 40.
# autor = " juan caraguay"
# email = " juan.p.caraguay@unl.edu.ec"

horas = float(input("Introduzca las horas:"))
tarifa= float(input("Introduzca la tarifa por hora:"))
if horas >=40:
    horas_xt = horas - 40
    tarifa_xt = (0.5 * tarifa) + tarifa
    salario = (tarifa_xt * horas_xt) + (horas * tarifa)
    print('su salario es de: ' , salario)
else:
    salario = tarifa * horas
    print('su salario es de: ' , salario)