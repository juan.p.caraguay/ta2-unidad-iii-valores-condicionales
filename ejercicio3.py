# Escribe un programa que solicite una puntuacion entre 0.0 y 1.0
# si la puntuacion esta fuera de ese rango muestra un mensaje de error
# si la puntuacion esta entre 0.0 y 1.0
# muestra la calificacion usando la siguiente tabla.

# autor  = "juan caraguay"
# email  = "juan.p.caraguay@unl.edu.ec"

try:
    puntuacion= float(input("introduzca puntuacion:"))
    # rango de puntuacion
    if puntuacion >= 0.9:
     print("sobresaliente")
    elif puntuacion >=0.8:
     print("notable")
    elif puntuacion >=0.7:
     print ("bien")
    elif puntuacion >=0.6:
     print("suficiente")
    elif puntuacion >=0.6:
     print("insuficiente")

    else:
        print("puntuacion incorrecta")
except:
    print("puntuacion incorrecta")